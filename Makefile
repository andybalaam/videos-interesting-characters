
NAME := interesting-characters


all:
	echo "try 'make upload'"

upload:
	rsync -r \
		--exclude .git \
		./ \
		dreamhost:artificialworlds.net/presentations/${NAME}/

zip:
	mkdir -p pkg
	- rm -rf pkg/interesting-characters
	mkdir -p pkg/interesting-characters
	cp -r *.html *.css *.js LICENSE images/ moreinfo/ pkg/interesting-characters/
	cd pkg && zip -r interesting-characters.zip interesting-characters
