import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.util.HashMap;
import java.util.Map;

/**
 * Charset for GSM 03.38 Basic Character Set and Basic Character Set
 * Extension, writing each 7-bit character as a separate byte with a
 * leading 0.
 *
 * Does NOT include national language shift tables.
 */
class Gsm0338BasicCharset extends Charset {
    public Gsm0338BasicCharset() {
        super("GSM_0338", new String[0]);
    }

    @Override
    public boolean canEncode() {
        return true;
    }

    @Override
    public boolean contains(Charset cs) {
        return (this == cs);
    }

    @Override
    public CharsetEncoder newEncoder() {
        return new Enc(this);
    }

    @Override
    public CharsetDecoder newDecoder() {
        return new Dec(this);
    }

    private static final Map<Integer, byte[]> encodeTable = makeEncodeTable();
    private static final Map<Byte, Integer> decodeTable =
        makeDecodeTable(encodeTable);
    private static final Map<Byte, Integer> decodeExtensionTable =
        makeDecodeExtensionTable(encodeTable);

    private static Map<Integer, byte[]> makeEncodeTable() {
        // From https://www.unicode.org/Public/MAPPINGS/ETSI/GSM0338.TXT
        Map<Integer, byte[]> ret = new HashMap<>();
        ret.put(0x0040, new byte[] {0x00});
        ret.put(0x00A3, new byte[] {0x01});
        ret.put(0x0024, new byte[] {0x02});
        ret.put(0x00A5, new byte[] {0x03});
        ret.put(0x00E8, new byte[] {0x04});
        ret.put(0x00E9, new byte[] {0x05});
        ret.put(0x00F9, new byte[] {0x06});
        ret.put(0x00EC, new byte[] {0x07});
        ret.put(0x00F2, new byte[] {0x08});
        ret.put(0x00E7, new byte[] {0x09});
        ret.put(0x000A, new byte[] {0x0A});
        ret.put(0x00D8, new byte[] {0x0B});
        ret.put(0x00F8, new byte[] {0x0C});
        ret.put(0x000D, new byte[] {0x0D});
        ret.put(0x00C5, new byte[] {0x0E});
        ret.put(0x00E5, new byte[] {0x0F});
        ret.put(0x0394, new byte[] {0x10});
        ret.put(0x005F, new byte[] {0x11});
        ret.put(0x03A6, new byte[] {0x12});
        ret.put(0x0393, new byte[] {0x13});
        ret.put(0x039B, new byte[] {0x14});
        ret.put(0x03A9, new byte[] {0x15});
        ret.put(0x03A0, new byte[] {0x16});
        ret.put(0x03A8, new byte[] {0x17});
        ret.put(0x03A3, new byte[] {0x18});
        ret.put(0x0398, new byte[] {0x19});
        ret.put(0x039E, new byte[] {0x1A});
        ret.put(0x00A0, new byte[] {0x1B});
        ret.put(0x000C, new byte[] {0x1B, 0x0A});
        ret.put(0x005E, new byte[] {0x1B, 0x14});
        ret.put(0x007B, new byte[] {0x1B, 0x28});
        ret.put(0x007D, new byte[] {0x1B, 0x29});
        ret.put(0x005C, new byte[] {0x1B, 0x2F});
        ret.put(0x005B, new byte[] {0x1B, 0x3C});
        ret.put(0x007E, new byte[] {0x1B, 0x3D});
        ret.put(0x005D, new byte[] {0x1B, 0x3E});
        ret.put(0x007C, new byte[] {0x1B, 0x40});
        ret.put(0x20AC, new byte[] {0x1B, 0x65});
        ret.put(0x00C6, new byte[] {0x1C});
        ret.put(0x00E6, new byte[] {0x1D});
        ret.put(0x00DF, new byte[] {0x1E});
        ret.put(0x00C9, new byte[] {0x1F});
        ret.put(0x0020, new byte[] {0x20});
        ret.put(0x0021, new byte[] {0x21});
        ret.put(0x0022, new byte[] {0x22});
        ret.put(0x0023, new byte[] {0x23});
        ret.put(0x00A4, new byte[] {0x24});
        ret.put(0x0025, new byte[] {0x25});
        ret.put(0x0026, new byte[] {0x26});
        ret.put(0x0027, new byte[] {0x27});
        ret.put(0x0028, new byte[] {0x28});
        ret.put(0x0029, new byte[] {0x29});
        ret.put(0x002A, new byte[] {0x2A});
        ret.put(0x002B, new byte[] {0x2B});
        ret.put(0x002C, new byte[] {0x2C});
        ret.put(0x002D, new byte[] {0x2D});
        ret.put(0x002E, new byte[] {0x2E});
        ret.put(0x002F, new byte[] {0x2F});
        ret.put(0x0030, new byte[] {0x30});
        ret.put(0x0031, new byte[] {0x31});
        ret.put(0x0032, new byte[] {0x32});
        ret.put(0x0033, new byte[] {0x33});
        ret.put(0x0034, new byte[] {0x34});
        ret.put(0x0035, new byte[] {0x35});
        ret.put(0x0036, new byte[] {0x36});
        ret.put(0x0037, new byte[] {0x37});
        ret.put(0x0038, new byte[] {0x38});
        ret.put(0x0039, new byte[] {0x39});
        ret.put(0x003A, new byte[] {0x3A});
        ret.put(0x003B, new byte[] {0x3B});
        ret.put(0x003C, new byte[] {0x3C});
        ret.put(0x003D, new byte[] {0x3D});
        ret.put(0x003E, new byte[] {0x3E});
        ret.put(0x003F, new byte[] {0x3F});
        ret.put(0x00A1, new byte[] {0x40});
        ret.put(0x0041, new byte[] {0x41});
        ret.put(0x0391, new byte[] {0x41});
        ret.put(0x0042, new byte[] {0x42});
        ret.put(0x0392, new byte[] {0x42});
        ret.put(0x0043, new byte[] {0x43});
        ret.put(0x0044, new byte[] {0x44});
        ret.put(0x0045, new byte[] {0x45});
        ret.put(0x0395, new byte[] {0x45});
        ret.put(0x0046, new byte[] {0x46});
        ret.put(0x0047, new byte[] {0x47});
        ret.put(0x0048, new byte[] {0x48});
        ret.put(0x0397, new byte[] {0x48});
        ret.put(0x0049, new byte[] {0x49});
        ret.put(0x0399, new byte[] {0x49});
        ret.put(0x004A, new byte[] {0x4A});
        ret.put(0x004B, new byte[] {0x4B});
        ret.put(0x039A, new byte[] {0x4B});
        ret.put(0x004C, new byte[] {0x4C});
        ret.put(0x004D, new byte[] {0x4D});
        ret.put(0x039C, new byte[] {0x4D});
        ret.put(0x004E, new byte[] {0x4E});
        ret.put(0x039D, new byte[] {0x4E});
        ret.put(0x004F, new byte[] {0x4F});
        ret.put(0x039F, new byte[] {0x4F});
        ret.put(0x0050, new byte[] {0x50});
        ret.put(0x03A1, new byte[] {0x50});
        ret.put(0x0051, new byte[] {0x51});
        ret.put(0x0052, new byte[] {0x52});
        ret.put(0x0053, new byte[] {0x53});
        ret.put(0x0054, new byte[] {0x54});
        ret.put(0x03A4, new byte[] {0x54});
        ret.put(0x0055, new byte[] {0x55});
        ret.put(0x0056, new byte[] {0x56});
        ret.put(0x0057, new byte[] {0x57});
        ret.put(0x0058, new byte[] {0x58});
        ret.put(0x03A7, new byte[] {0x58});
        ret.put(0x0059, new byte[] {0x59});
        ret.put(0x03A5, new byte[] {0x59});
        ret.put(0x005A, new byte[] {0x5A});
        ret.put(0x0396, new byte[] {0x5A});
        ret.put(0x00C4, new byte[] {0x5B});
        ret.put(0x00D6, new byte[] {0x5C});
        ret.put(0x00D1, new byte[] {0x5D});
        ret.put(0x00DC, new byte[] {0x5E});
        ret.put(0x00A7, new byte[] {0x5F});
        ret.put(0x00BF, new byte[] {0x60});
        ret.put(0x0061, new byte[] {0x61});
        ret.put(0x0062, new byte[] {0x62});
        ret.put(0x0063, new byte[] {0x63});
        ret.put(0x0064, new byte[] {0x64});
        ret.put(0x0065, new byte[] {0x65});
        ret.put(0x0066, new byte[] {0x66});
        ret.put(0x0067, new byte[] {0x67});
        ret.put(0x0068, new byte[] {0x68});
        ret.put(0x0069, new byte[] {0x69});
        ret.put(0x006A, new byte[] {0x6A});
        ret.put(0x006B, new byte[] {0x6B});
        ret.put(0x006C, new byte[] {0x6C});
        ret.put(0x006D, new byte[] {0x6D});
        ret.put(0x006E, new byte[] {0x6E});
        ret.put(0x006F, new byte[] {0x6F});
        ret.put(0x0070, new byte[] {0x70});
        ret.put(0x0071, new byte[] {0x71});
        ret.put(0x0072, new byte[] {0x72});
        ret.put(0x0073, new byte[] {0x73});
        ret.put(0x0074, new byte[] {0x74});
        ret.put(0x0075, new byte[] {0x75});
        ret.put(0x0076, new byte[] {0x76});
        ret.put(0x0077, new byte[] {0x77});
        ret.put(0x0078, new byte[] {0x78});
        ret.put(0x0079, new byte[] {0x79});
        ret.put(0x007A, new byte[] {0x7A});
        ret.put(0x00E4, new byte[] {0x7B});
        ret.put(0x00F6, new byte[] {0x7C});
        ret.put(0x00F1, new byte[] {0x7D});
        ret.put(0x00FC, new byte[] {0x7E});
        ret.put(0x00E0, new byte[] {0x7F});
        return ret;
    }

    private static Map<Byte, Integer> makeDecodeTable(
            Map<Integer, byte[]> encodeTable
    ) {
        Map<Byte, Integer> ret = new HashMap<>();
        for (Map.Entry<Integer, byte[]> entry : encodeTable.entrySet()) {
            if (entry.getValue().length == 1) {
                ret.put(entry.getValue()[0], entry.getKey());
            }
        }
        return ret;
    }

    private static Map<Byte, Integer> makeDecodeExtensionTable(
            Map<Integer, byte[]> encodeTable
    ) {
        Map<Byte, Integer> ret = new HashMap<>();
        for (Map.Entry<Integer, byte[]> entry : encodeTable.entrySet()) {
            if (entry.getValue().length == 2) {
                assert entry.getValue()[0] == 0x1B;
                ret.put(entry.getValue()[1], entry.getKey());
            }
        }
        return ret;
    }

    private static class Dec extends CharsetDecoder {
        private static class DecodingFailed extends Exception {
            public final CoderResult result;
            private DecodingFailed(CoderResult result) {
                this.result = result;
            }
        }

        Dec(Charset cs) {
            super(cs, 1.0f, 1.0f);
        }

        @Override
        public CoderResult decodeLoop(ByteBuffer in, CharBuffer out) {
            while (in.hasRemaining()) {
                byte b = in.get();
                try {
                    dec(b, in, out);
                }
                catch (DecodingFailed e) {
                    // If we failed to decode, we should back up the
                    // input buffer, so the calling code can use it
                    // to report the failure, or it is given back to
                    // us next time we are called.
                    return e.result;
                }
                return CoderResult.UNDERFLOW;
            }
            return CoderResult.UNDERFLOW;
        }

        private void dec(
            byte b,
            ByteBuffer in,
            CharBuffer out
        ) throws DecodingFailed {
            if (b == 0x1B) { // This "ESC", so we are expecting another byte
                if (!in.hasRemaining()) {
                    // We didn't decode anything, so we back up here -
                    // not sure whether that is ok.
                    throw new DecodingFailed(CoderResult.UNDERFLOW);
                }
                byte extB = in.get();
                put(decodeExtensionTable.get(extB), out);
            } else {
                put(decodeTable.get(b), out);
            }
        }

        private static void put(
            Integer ch,
            CharBuffer out
        ) throws DecodingFailed {
            if (ch == null) {
                throw new DecodingFailed(CoderResult.unmappableForLength(1));
            } else {
                if (!out.hasRemaining()) {
                    throw new DecodingFailed(CoderResult.OVERFLOW);
                }
                out.put((char)ch.intValue());
            }
        }
    }

    private static class Enc extends CharsetEncoder {
        private enum Utf16UnitType {
            SINGLE_CHARACTER,
            HIGH_SURROGATE,
            LOW_SURROGATE,
        }

        private static class EncodingFailed extends Exception {
            public final CoderResult result;
            private EncodingFailed(CoderResult result) {
                this.result = result;
            }
        }

        Enc(Charset cs) {
            super(cs, 2.0f, 2.0f);
        }

        @Override
        public CoderResult encodeLoop(CharBuffer in, ByteBuffer out) {
            while (in.hasRemaining()) {
                char ch = in.get();
                try {
                    enc(ch, out);
                }
                catch (EncodingFailed e) {
                    // If we failed to encode, we should back up the
                    // input buffer, so the calling code can use it
                    // to report the failure.
                    in.position(in.position() - 1);
                    return e.result;
                }
            }
            return CoderResult.UNDERFLOW;
        }

        private void enc(char ch, ByteBuffer out) throws EncodingFailed {
            // ch is a UTF-16 code unit.
            switch (typeOfUtf16CodeUnit(ch)) {
                case SINGLE_CHARACTER:
                    singleChar(ch, out);
                    break;
                case HIGH_SURROGATE:
                    throw new EncodingFailed(
                        CoderResult.unmappableForLength(2));
                case LOW_SURROGATE:
                    // The line above means we should have skipped
                    // all low surrogates
                    throw new EncodingFailed(CoderResult.malformedForLength(1));
            }
        }

        private static Utf16UnitType typeOfUtf16CodeUnit(char c) {
            if (c >= 0xD800 && c < 0xDC00) {
                return Utf16UnitType.HIGH_SURROGATE;
            } else if (c >= 0xDC00 && c < 0xE000) {
                return Utf16UnitType.LOW_SURROGATE;
            } else {
                return Utf16UnitType.SINGLE_CHARACTER;
            }
        }

        private static void singleChar(
            char ch,
            ByteBuffer out
        ) throws EncodingFailed {
            byte[] bs = encodeTable.get(Integer.valueOf(ch));
            if (bs == null) {
                throw new EncodingFailed(CoderResult.unmappableForLength(1));
            } else {
                for (byte b : bs) {
                    if (!out.hasRemaining()) {
                        throw new EncodingFailed(CoderResult.OVERFLOW);
                    }
                    out.put(b);
                }
            }
        }
    }
}
