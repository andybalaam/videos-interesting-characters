import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

public class Chars {
    private static final Charset gsm0338BasicCharset =
        new Gsm0338BasicCharset();

    public static void main(String[] args) throws Exception {
        //for (String name: Charset.availableCharsets().keySet()) {
        //    System.out.println(name);
        //}
        System.out.println();
        info("A");      // U+0041 LATIN CAPITAL LETTER A
        info("{");      // 
        info("\u00E9"); // U+00e9 LATIN SMALL LETTER E WITH ACUTE
        info("\u03A9"); // U+03A9 GREEK CAPITAL LETTER OMEGA
        info("\u2E9F"); // U+2E9F CJK RADICAL MOTHER
        info("\uFE18"); // U+FE18 PRESENTATION FORM FOR VERTICAL RIGHT WHITE LENTICULAR BRAKCET

        // U+012F LATIN SMALL LETTER I WITH OGONEK
        // U+0307 COMBINING DOT ABOVE
        // U+0301 COMBINING ACUTE ACCENT
        info("\u012F\u0307\u0301");

        info("\uD83D\uDCA9"); // U+1F4A9 PILE OF POO

        // U+1F9D1 ADULT
        // U+1F3FF EMOJI MODIFIER FITZPATRICK TYPE-6
        info("\uD83E\uDDD1\uD83C\uDFFF");
    }

    private static void info(String ch) throws Exception {
        System.out.println(
            String.format("%11s: %s  %s", "Character", ch, codePoints(ch))
        );
        printEncoded("UTF-8", ch);
        printEncoded("UTF-16BE", ch);
        printEncoded("UTF-16LE", ch);
        printEncoded("UTF-32", ch);
        printEncoded("US-ASCII", ch);
        printEncoded("ISO-8859-1", ch);
        printEncoded("ISO-8859-7", ch);
        printEncoded("GB18030", ch);
        printEncoded("Shift_JIS", ch);
        printEncoded("GSM_0338", ch);
        printEncoded("IBM1047", "EBCDIC 1047", ch);
        System.out.println();
    }

    private static String codePoints(String ch) {
        return ch.codePoints().mapToObj(
            i ->
                String.format(
                    "U+%4s",
                    Integer.toHexString(i).toUpperCase()
                ).replaceAll(" ", "0")
                + " "
                + Character.getName(i)

        ).collect(Collectors.joining(" "));
    }

    private static void printEncoded(String charsetName, String str) {
        printEncoded(charsetName, charsetName, str);
    }

    private static void printEncoded(
            String charsetName,
            String printName,
            String str
    ) {
        ByteBuffer enc = encode(charsetName, str);
        /*String dec = */decode(charsetName, enc.duplicate()).toString();
        System.out.println(
            String.format("%11s", printName)
            + ":"
            + showBytes(charsetName, enc)
            // + " "
            // + dec
        );
    }

    private static Charset charsetForName(String charsetName) {
        Charset cs = null;
        if (charsetName.equals(gsm0338BasicCharset.name())) {
            cs = gsm0338BasicCharset;
        } else {
            cs = Charset.forName(charsetName);
        }
        return cs;
    }

    private static ByteBuffer encode(String charsetName, String str) {
        return charsetForName(charsetName).encode(str);
    }

    private static CharBuffer decode(String charsetName, ByteBuffer bytes) {
        return charsetForName(charsetName).decode(bytes);
    }

    private static String showBytes(String charsetName, ByteBuffer bytes) {
        if (bytes.equals(ByteBuffer.wrap(new byte[] {0x3F}))) {
            return " -";
        }

        StringBuilder ret = new StringBuilder();
        int toFourBytes = 4;
        while (bytes.hasRemaining()) {
            --toFourBytes;
            byte b = bytes.get();
            ret.append(" " + asHex(b));
        }

        for (int i = 0; i < toFourBytes; ++i) {
            ret.append("   ");
        }

        ret.append(" ");
        bytes.rewind();
        while (bytes.hasRemaining()) {
            byte b = bytes.get();
            ret.append(" " + asBinary(b));
        }

        return ret.toString();
    }

    private static String asBinary(byte b) {
        int i = b & 0xFF;  // The byte as a positive integer
        return String.format(
            "%8s",
            Integer.toBinaryString(i)
        ).replaceAll(" ", "0");
    }

    private static String asHex(byte b) {
        int i = b & 0xFF;  // The byte as a positive integer
        return String.format("%02X", i);
    }
}
